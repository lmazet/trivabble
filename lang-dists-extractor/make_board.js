#!/usr/bin/env node
// vim: noai:ts=4:sw=4

/* eslint-disable no-multi-str */
/* eslint-disable no-process-env */
/* eslint-disable no-sync */

const ROOT = process.env.ROOT_BOARD || ".";

const fs = require("fs");
const path = require("path");

/* The French wikipedia page is easier to parse */
const htmlText = fs.readFileSync("Lettres_du_Scrabble", {encoding: "utf-8"});

/* Table used to translate language name from French to English */
const code = require(path.join(__dirname, "languageCodes.json")); // eslint-disable-line global-require

let lang;
let key;
let bag = "";
let values = "";
let nbJockers = 0;

for (const line of htmlText.split("\n")) {

    /* Search language */
    if (line.match(/<h2.*mw-headline/u)) {

        /* Create previous language board definition */
        if (bag.length > 1) {
            bag = bag.slice(0, -2);
            values = values.slice(0, -2) + "\n";

            /* Board file */
            let file = "{\n";
            file += "    \"code\": \"" + key + "\",\n";
            file += "    \"name\": \"" + lang + "\",\n";
            file += "\n";
            file += "    \"bag\": [\n" + bag + "\n    ],\n";
            file += "\n";
            file += "    \"letterValues\": {\n";
            file += values;
            file += "    }\n";
            file += "}\n";
            fs.writeFileSync(path.join(ROOT, key + ".json"), file);
        }

        /* Get language name */
        const regexpLang = /mw-headline"[^<]*">(?<lang>[^<]*)</gu;
        lang = regexpLang.exec(line).groups.lang;

        /* As parsing has been done on the French page, one needs to
           translate language name from French to English */
        for (const k of Object.keys(code.langFr)) {
            if (code.langFr[k] === lang) {
                lang = code.langEn[k];
                key = k;
                break;
            }
        }

        /* Empty bag */
        bag = values = "";
        nbJockers = 0;
    }

    /* Search tile */
    if (line.match(/<li.*<\/li>/u) && (nbJockers < 2)) {

        /* Search value */
        const regexpValue = /i>(?<value>[^<]*) point/gu;
        let value = regexpValue.exec(line);
        if (value) {
            value = value.groups.value;
        }

        /* Search letters */
        const regexpTiles = /<b>(?<letter>[^<]*)<\/b> [^<]*<small>[×x](?<times>[^<,]*),*<\/small>/gu;
        let matches;
        const tiles = [];
        while ((matches = regexpTiles.exec(line))) {
            tiles.push(matches.groups);
        }

        /* Create bag and values table */
        for (let i = 0; i < tiles.length; i++) {

            if (tiles[i].letter.length > 2) {
                tiles[i].letter = " ";
                nbJockers++;
                if (nbJockers === 2) {
                    break;
                }
            }
            
            bag += "       ";
            for (let j = 0; j < tiles[i].times; j++) {
                bag += " \"" + tiles[i].letter + "\",";
            }
            bag += "\n";
            values += "        \"" + tiles[i].letter + "\": " + value + ",\n";
        }
    }
}
