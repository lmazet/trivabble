La version officielle du Scrabble en ukrainien a été commercialisée en 2010. Elle utilise l'alphabet cyrillique et compte 102 jetons.

- 0 point: Jokers ×2
- 1 point: О ×9, А ×8, И ×6, Н ×6, В ×5, Е ×5, І ×5, Т ×5
- 2 points: К ×4, Р ×4, С ×4
- 3 points: Д ×3, Л ×3, М ×3, У ×3
- 4 points: П ×3, З ×2, Я ×2, Ь ×2
- 5 points: Б ×2, Г ×2, Ч ×2, Х ×1
- 8 points: Є ×1, Ї ×1, Й ×1, Ж ×1, Ц ×1, Ш ×1, Ю ×1
- 10 points: Ґ ×1, Ф ×1, Щ ×1, ‍'‍ ×1

Le signe d'apostrophe est également inclus, même s'il ne s'agit pas d'une lettre de l'alphabet ukrainien. 
