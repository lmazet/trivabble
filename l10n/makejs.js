#!/usr/bin/env node
/*eslint strict: [2, "global"]*/
/*eslint no-sync: ["error", { allowAtRootLevel: true }]*/

"use strict";

const ROOT = "../public/l10n/js";

/*  Builds translation files. */

const fs = require("fs");
const path = require("path");

let po;
let i;
let len;

function skipLine() {
    while (i < len && po[i] !== "\n") {
        ++i;
    }
    ++i;
}

function skipSpaces() {
    while (i < len && !po[i].trim()) {
        ++i;
    }
}

function parseString() {
    skipSpaces();
    if (po[i] !== '"') {
        return "";
    }
    ++i;
    const deb = i;

    while (i < len) {
        if (po[i] === "\\") {
            ++i;
        } else if (po[i] === '"') {
            const str1 = po.substring(deb, i++);
            const end = i;
            skipSpaces();
            const ndeb = i;
            const str2 = parseString();

            if (i === ndeb) { // we did not parse anything
                i = end;
                return str1;
            }

            return str1 + str2;
        }
        ++i;
    }
    throw new Error("not ended string at character " + deb);
}

let msgid;
let msgstr;

fs.mkdirSync(ROOT, {recursive: true});

for (const lang of fs.readdirSync("po")) {
    const jsFile = fs.openSync(path.join(ROOT, lang + ".js"), "w");
    fs.writeSync(jsFile, "(function(){var ");
    let translationFunction = "translationFunction";

    for (const poFile of fs.readdirSync("po/" + lang)) {
        translationFunction = fs.readFileSync("pot/" + poFile + "t", {encoding: "utf-8"})
            .match(/#TranslationFunction[\s]+(?<functionName>[\S]+)/u).groups.functionName;

        fs.writeSync(jsFile, "_=" + translationFunction + ".l10n;");

        po = fs.readFileSync("po/" + lang + "/" + poFile, {encoding: "utf-8"});

        i = 0;
        len = po.length;
        while (i < len) {
            skipSpaces();

            if (po.substr(i, 5) === "msgid") {
                if (po[i + 5].trim() && po[i + 5] !== '"') {
                    skipLine(); // don't understand this line
                } else {
                    i += 5;
                    skipSpaces();
                    msgid = parseString();
                }
            } else if (po.substr(i, 6) === "msgstr") {
                if (po[i + 6].trim() && po[i + 6] !== '"') {
                    skipLine(); // don't understand this line
                } else {
                    i += 6;
                    msgstr = parseString();
                    fs.writeSync(
                        jsFile,
                        '_("' + lang + '","' + msgid.replace(/\n/gu, "") + '","' + msgstr.replace(/\n/gu, "") + '");'
                    );
                }
            }

            // if po[i] === "#", ignore
            skipLine();
        }
    }

    fs.writeSync(
        jsFile,
        "if(" + translationFunction + ".applyL10n){" + translationFunction + ".applyL10n();}})();"
    );

    fs.close(jsFile, function (e) {
        if (e) {
            console.error(e);
        }
    });
}
