msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"
"Last-Translator: \n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"Language: br\n"

# TranslationFunction trivabble
msgid "Your game is not over. Are you sure you want to leave now?"
msgstr "N'eo ket echu ho c'hoariadenn. Sur oc'h e fell deoc'h kuitaat bremañ?"

msgid ""
"You are about to leave the current game. To recover it, please note its "
"number: {0}"
msgstr ""
"Emaoc'h o vont da guitaat ar c'hoariadenn. Evit hec'h adkavout e c'hallit "
"notenniñ an niverenn da heul: {0}"

msgid ""
"Sorry, a problem just happened. The page must be reloaded. If the problem is "
"not too serious, you should be able to keep playing normally. Otherwise, "
"contact the person who is able to fix the problem. Click on “Yes” to reload "
"the page."
msgstr ""
"Digarezit, ur gudenn a zo degouezhet. Ret eo adkargañ ar bajenn. Ma n'eo ket "
"re bouezus ar gudenn e o tu deoc'h kenderc'hel da c'hoari. E mod all, kit e "
"darempred gant drouizig@drouizig.org evit kempenn ar gudenn. Klikit war \"Ya"
"\" evit adkargañ ar bajenn."

msgid ""
"To join a game, please give the number which is displayed on your "
"adversary(ies)' screen.\n"
"If you do not know it, ask them.\n"
"\n"
"Warning: your adversary must not take your number, (s)he must keep his/her "
"own. If you whish to recover your current game, please not the following "
"number: {0}."
msgstr ""
"Evit kevreañ d'ur c'hoariadenn, roit an niverenn diskouezet war skramm ho "
"enebour(ien).\n"
"Ma n'anavezit ket anezhañ, goulennit ganto.\n"
"\n"
"Diwallit: ne rank ket hoc'h enebour kemer ho niverenn, ret eo dezhañ pe "
"dezhi derc'hel (h)e hini. Ma fell deoc'h adkavout ho c'hoari bremanel, "
"notennit an niverenn da heul: {0}."

msgid ""
"It seems your did not give a correct number, or you clicked on “Cancel”. As "
"a result, the current game continues, if any. To join a game, click on “Join "
"a game” again."
msgstr ""
"War a seblant n'ho peus ket roet un niverenn dalvoudek, p ekliket ho peus "
"war \"Nullañ\". Kendalc'het e vo gant ar c'hoari bremanel. Evit kevreañ d'ur "
"c'hoari, klikit war \"Kevreañ d'ur c'hoari\" adarre."

msgid ""
"To change your name, enter a new one. You can keep using your current name "
"by cancelling. Please note that if you change your name and you have games "
"in progress, you will not be able to keep playing them anymore unless you "
"get back to your current name."
msgstr ""
"Evit kemmañ hoc'h anv, enankit unan nevez. Gallout a rit derc'hel hoc'h hini "
"bremanel en ur glikañ war \"Nullañ\". Diwallit: ma kemmit hoc'h anv en ur "
"c'hoari, ne vo ket tu deoc'h kenderc'hel da c'hoari estreget mar adlakit "
"hoc'h anv bremanel en-dro."

msgid "You cannot take another tile: your rack is full."
msgstr "N'hallit ket kement ur pezh all. Leun eo ho touger-pezhioù."

msgid ""
"Are you sure you want to put all the tiles back in the bag (in order to play "
"another game)?"
msgstr ""
"Sur oc'h e fell deoc'h adlakaat an holl bezhioù er sac'h (evit c'hoari en-"
"dro)?"

msgid "Are you sure you want to put all your tiles back in the bag?"
msgstr "Sur oc'h e fell deoc'h adlakaat holl ho pezhio er sac'h?"

msgid ""
"It seems your did not give your name. You need to do it for the game to run "
"properly."
msgstr ""
"War a-seblant n'ho peus ket roet hoc'h anv. et eo deoc'h en ober evit "
"gellout c'hoari."

msgid ""
"Hello! To begin, enter your name. Your adversaries will see this name when "
"you play with them."
msgstr ""
"Demat! Evit kregiñ da c'hoari, enankit hoc'h anv. Hoc'h enebourien a welo an "
"anv pa viot o c'hoari gante."

msgid ""
"Double\n"
"Letter"
msgstr ""
"Lizherenn\n"
"Zoubl"

msgid ""
"Double\n"
"Word"
msgstr ""
"Ger\n"
"Doubl"

msgid ""
"Triple\n"
"Letter"
msgstr ""
"Lizherenn\n"
"Dripl"

msgid ""
"Triple\n"
"Word"
msgstr ""
"Ger\n"
"Tripl"

msgid "Join adversaries"
msgstr "Kevreañ gant enebourien"

msgid "You are:"
msgstr "Hoc'h anv:"

msgid "Change"
msgstr "Kemmañ"

msgid "(name to give)"
msgstr "(anv da reiñ)"

msgid ""
"Put back all the tiles\n"
"in the bag"
msgstr "Adlakaat an holl pezhio er sac'h"

msgid "Number of your game:"
msgstr "Niverenn ho c'hoari:"

msgid "(pending)"
msgstr "(o c'hortoz)"

msgid "Write a message to your adversaries here"
msgstr "Skrivit kemennadenno d'hoc'h enebourien aze"

msgid "Send message"
msgstr "Kas ar gemennadenn"

msgid "Sound of the tiles"
msgstr "Son ar pezhioù"

msgid "Sound of messages"
msgstr "Son ar c'hemennadenoù"

msgid "Number of tiles in the bag:"
msgstr "Niver a bezhioù er sac'h:"

msgid "Click on it to take one."
msgstr "Klikit warnañ evit kemer unan"

msgid "Put back all the tiles in the bag"
msgstr "Adlakaat an holl pezhioù er sac'h"

msgid "Participant"
msgstr "C'hoarier"

msgid "Rack"
msgstr "Douger-pezhioù"

msgid "Score"
msgstr "Poentoù"

msgid "Waiting for other participants…"
msgstr "O c'hortoz c'hoarien all..."

msgid ""
"Click on someone's score\n"
"to change it."
msgstr ""
"Klikit war poentoù unan bennak\n"
"evit kemmañ."

#. The part of the sentence before and after line break should not
#. be longer than the original message.
#. It is okay to put line breaks anywhere to split the sentence so it is not
#. wider than the original sentence.
msgid ""
"Put back all the tiles of\n"
"your rack in the bag"
msgstr ""
"Adlakaat holl pezhioù ho\n"
"touger-pezhoù er sac'h"

msgid  "Interface language:"
msgstr "Yezh ar c'hetal:"
