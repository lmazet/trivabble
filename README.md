# Trivabble

Trivabble is free a network Scrabble game under the (AGPLv3 license)[LICENSE].
It is not in any way connected to Hasbro, who has the rights on the trademark Scrabble.
Trivabble provides a board, a bag full of tiles, racks and a chat.
People can play without registering and join each other by using the same game number.

It features:
 - a board, a bag full of tiles, a rack for every player
 - a chat so people can communicate
 - a way to set the score of each player
 - support for different languages (set of tiles as well as the interface). Thanks to Wikipedia for providing data about tiles for each language (https://fr.wikipedia.org/wiki/Lettres_du_Scrabble)

Trivabble does not implement any rule: you are free to play your way.
It does not understand what you do and is not able to compute the score, like your real board game.

Trivabble needs to be installed on a server, and is playable from a browser.

It supports:
 - latest versions of Firefox and Chromium
 - Mobile browsers on Android
 - Safari 9 and later on the iPad

It should work on any browser released after 2015, though we haven't tested it on Internet Explorer.

This documentation is intented for people who want to install Trivabble on their server.
If you just want to play, head to https://trivabble.1s.fr/.

For contributions, see (contributing)[CONTRIBUTING.md].

## Set up for production

Note: Please be aware that this is alpha-quality software.
We do our best to avoid breaking stuff but we might from time to time.
With this out of the way…

Trivabble is composed of two parts: the server and the client.

- The server is a Javascript program that runs on Node JS.
  It waits for HTTP and WebSocket requests on a configurable port (default: 3000), and a configurable address (default: localhost).
  This server needs to run for Trivabble to work, and proxified through a Web server like Apache or Nginx.
- The client is a set of static files that need to be server by a Web server

### Prerequisite

You need to have:
 - Node (version 10 or later)
 - make
 - git
 - a Web server such as Apache or Nginx.

On a Debian-based system, the following command will install make and node:

    apt install make nodejs git

On some systems, the node binary does not exist and is called `nodejs` instead. Trivabble expects the Node binary to be `node`. On these systems, a `nodejs-legacy` package may exist to fix this. Otherwise, you will need to create a symblink.

As for the web server, we will asume that you already have one running and correctly configured.

### Installation

We will assume www-data is the UNIX user of your Web server. Adapt if necessary.

1. Set up the directories that will host the server and the client.
   Create a UNIX user for the Trivabble server and set the right permission:
   The server should be outside the root of your Web server.
   The client will be served by your Web server.

   Let's store the chosen paths in environment variables (adapt if necessary) and create these directories:

   ```sh
   export TRIVABBLE_SERVER_PATH=/opt/trivabble-server
   export TRIVABBLE_PUBLIC_PATH=/var/www/html/trivabble

   sudo groupadd trivabble
   sudo useradd -G trivabble trivabble
   sudo mkdir -p "$TRIVABBLE_PUBLIC_PATH"
   sudo chown www-data:www-data "$TRIVABBLE_PUBLIC_PATH"
   sudo chmod g+x "$TRIVABBLE_PUBLIC_PATH"
   sudo mkdir -p "$TRIVABBLE_SERVER_PATH"
   sudo chown trivabble:trivabble "$TRIVABBLE_PUBLIC_PATH"
   ```

2. Clone the repository somewhere (in your HOME directory for instance):

    ```sh
    git clone https://gitlab.com/raphj/trivabble.git
    cd trivabble
    ```

3. Get the latest production version:

   ```sh
   git checkout "$(git tag --list '[prod]*' --sort=v:refname | tail -1)"
    ```

4. Make sure you still have your environment variable set (adapt if necessary):

   ```sh
   export TRIVABBLE_SERVER_PATH=/opt/trivabble-server
   export TRIVABBLE_PUBLIC_PATH=/var/www/html/trivabble
    ```

   Also make sure you still are in the trivabble repository.

5. Run, as a user having sudo privileges:

    ```sh
    ./bin/upgrade-prod.sh --prod-public-dir "$TRIVABBLE_PUBLIC_PATH" --prod-server-dir "$TRIVABBLE_SERVER_PATH" --trivabble-chown trivabble:trivabble --webserver-chown www-data:www-data
    ```

    This will:
     - run `make` so everything that needs to be build will be built, including the language files
     - transform Javascript files so they are compatible with older browsers
     - copy the server and client files to the chosen folders

6. (Optional) - In the public directory (`$TRIVABBLE_PUBLIC_PATH`), review `config.js.sample`. If there are values you would like to change, copy it to config.js and make the desired changes.

7. Run the Trivabble server.

    ```sh
    cd "$TRIVABBLE_SERVER_PATH"
    node trivabble-server
    ```

    You may want to run this in a `screen` so you can leave the session and trivabble-server still runs in the background.

    Better yet, create a service for your init system:

    - create `/usr/local/bin/start-trivabble`:

        ```
        #!/bin/sh
        cd /var/www/html/raph/raw/trivabble
        exec sudo -u www-data DEBUG_LOG=true nodejs trivabble-server.js
        ```

8. Configure your web server.

On Apache 2,  the relevant rules are:

    RewriteEngine on
    RewriteRule "^/?:trivabble/ws/(.*)" ws://localhost:3000/ws/$1 [P]

    ProxyPass "/:trivabble" http://localhost:3000/ retry=0
    ProxyPassReverse "/:trivabble" http://localhost:3000/

You will need to enable modules `proxy_http`, `proxy_wstunnel` (if you do not disable websockets in `config.js`) and `rewrite`:

```sh
a2enmod proxy_http
a2enmod proxy_wstunnel
a2enmod rewrite
```

A full example is available in (doc/trivabble-apache-host.conf.sample)[doc/trivabble-apache-host.conf.sample].

:warning: Be careful to allow enough concurrent processes / threads / workers on your web server.
Each player requires one long-standing connection (XHR, SSE or WebSocket), plus small requests when not using WebSockets.
