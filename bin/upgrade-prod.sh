#!/usr/bin/env sh

set -euf

error() {
    >&2 echo "$1"
    exit 1
}

help() {
    cat <<EOF
$0: - update the production.

This program updates Trivabble to the latest version tagged for production, and copies and prepares Trivabble for use in production.
Trivabble is composed of two parts: the server and the client.
The client is to be served by a regular HTTP server and lives in the public folder.
The server is to be stored somewhere on your system that is not served by your HTTP server.

Parameters:
    --prod-public-dir DIR: the path to the public directory, from which your HTTP server will serve the client
    --prod-server-dir DIR: the path to the server directory, not served by your HTTP server.
    --webserver-chown USER: the user:group chown string to use for the webserver directory
    --trivabble-chown USER: the user:group chown string to use for the server directory
    --no-git: do not attempt to fetch the last production revision
    --allow-root: allow running the script as root. Must be the first parameter.
EOF
    exit 0
}

if [ "$(id -u)" = "0" ]; then
    if [ "$1" = "--allow-root" ]; then
        shift
    else
        error "Not running as root. Please run me as a normal user with sudo rights, using something like sudo -H -u user $0 $@"
    fi
fi

NO_GIT=false

while ! [ -z "${1+x}" ]; do
    if [ "$1" = "--allow-root" ]; then
        shift
    elif [ "$1" = "--prod-public-dir" ]; then
        export PROD_PUBLIC_DIR=$2
        shift
        shift
    elif [ "$1" = "--prod-server-dir" ]; then
        export PROD_SERVER_DIR=$2
        shift
        shift
    elif [ "$1" = "--trivabble-chown" ]; then
        export TRIVABBLE_CHOWN=$2
        shift
        shift
    elif [ "$1" = "--webserver-chown" ]; then
        export WEBSERVER_CHOWN=$2
        shift
        shift
    elif [ "$1" = "--no-git" ]; then
        export NO_GIT=true
        shift
    elif [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
        help
    else
        error "Unrecognized argument $1. Type --help for more information."
    fi
done

if [ -z "${PROD_SERVER_DIR+x}" ]; then
   error "The server production directory is not set. Type --help for more information."
fi

if [ -z "${PROD_PUBLIC_DIR+x}" ]; then
   error "The public production directory is not set. Type --help for more information."
fi

if [ -z "${TRIVABBLE_CHOWN+x}" ]; then
   error "The trivabble chown string is not set. Type --help for more information."
fi

if [ -z "${WEBSERVER_CHOWN+x}" ]; then
   error "The webserver chown string is not set. Type --help for more information."
fi

if ! [ -d "$PROD_SERVER_DIR" ]; then
   error "The server production directory $PROD_SERVER_DIR does not exist. I give up."
fi

if ! [ -d "$PROD_PUBLIC_DIR" ]; then
   error "The public production directory $PROD_PUBLIC_DIR does not exist. I give up."
fi

WORKING_DIRECTORY="$(dirname "$(dirname "$0")")"

if ! [ -d "$WORKING_DIRECTORY/.git" ]; then
    error "Not a git repository. Cannot update."
fi

if [ -f "$WORKING_DIRECTORY/server/games.backup.json" ] || [ -f "$WORKING_DIRECTORY/games.backup.json" ]; then
    error "Your working copy has a 'games.backup.json' file. I give up."
fi

if ! [ -z "$(git status --porcelain)" ]; then
    error "Your git repository is not clean. I am not doing anything."
fi

# Thx https://stackoverflow.com/questions/17414104/git-checkout-latest-tag

if ! [ "${NO_GIT}" = "true" ]; then
    echo "Fetching updates from git"
    git fetch
    new_version="$(git tag --list '[prod]*' --sort=v:refname | tail -1)"

    echo "Checking out ${new_version}..."
    git checkout "$new_version"

    # Re-execute the updated upgrade-prod.sh script
    # Relies on environment variables being set in the current run
    exec "$0" --allow-root --no-git
fi

echo "Setting up production mode..."
"$(dirname $0)"/setup_prod.sh

echo "Sending to production..."
sudo rsync --chown="$WEBSERVER_CHOWN" -O -avp --delete --exclude 'config.js' "$WORKING_DIRECTORY/public/" "$PROD_PUBLIC_DIR/"
sudo rsync --chown="$TRIVABBLE_CHOWN" -O -avp --delete --exclude 'games.backup.json' "$WORKING_DIRECTORY/server/" "$PROD_SERVER_DIR/"

echo echo "Resetting the directory..."
git reset --hard HEAD
