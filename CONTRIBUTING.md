You are welcome to contribute to Trivabble to the code, the documentation, graphics, design, or any other aspect.
The more we are working on the project, the funnier.
However, the current maintainer is grumpy and opinionated.

# Discuss with us, and then work

As a consequence, you are highly encouraged to open an issue in the bug tracker to tell us what you want to work on so:

- we can avoid duplicate effort and discovering at the last moment two people were working on the same feature at the same time;
- we can tell you if we agree with this addition / modification. We would hate to have to reject your work after the facts.
  This is really important: when a feature is added, it is forever. We often can't remove a feature without being hated by our users.
  See the really good talk of Lichess's original author.
  We also want to keep the UI uncluttered and dead simple to figure out.
  Trivabble is used by people who are not into computers *at all*.
- we can discuss the implementation together to increase the chances your code will be merged.

Trivabble is currently hosted at gitlab.com. You are not forced to register on Gitlab.com or use to contribute to Trivabble.
Git works well by mail, so you are free to find the maintainer's email and discuss with them and send a patch or a link to a git branch hosted somewhere here.
However, if you do so, everybody might be missing out on collective thinking.

## Recommended flow

1. Open a ticket.
2. We discuss.
3. You work on whatever you want to work.
4. If you work on the code or the documentation, open a merge request whenever you want. Clearly tell us whether your branch is ready for review, or not so nobody wastes their time reviewing something that will change a lot. But we are keen to give early / continuous feedback if you'd like to have some ☺. We can also share a branch if needed.

## Risky flow

Contributing should be fun.
You can still work on your own, fiddle with the code and open a merge request or send patches when you think we should have a look.
But be aware that we could refuse to merge your work if we think the stars aren't aligned or for any other dumb or reasonable reason.

# Code Management and Coding style

## Linting

Please lint your code. Run `make eslint` before committing.
We use a custom set of rules.
The specific coding style does not really matter but we need uniformity.
Four spaces, mandatory semicolon, spaces after statement keywords, spaces around infix operators, and you should be good for the most part.
ESLint will warn you if you do something a bit off.
This frees us from speaking about coding styles when merging code, while still guaranteeing some uniformity.

## No advanced JavaScript features for the client side

The code needs to work on Safari 9.
Use `let` and `const` to declare variables. They are replaced by `var` with `sed` to make it work on this ancient browser.
It's hacky, we are parsing Javascript like Chtulus, but it works well in practice.
It's tested during development because the development server also rewrites Javascript using the same regular expression, so it cannot go really wrong.
Any error will be spotted before going to production.

Other than that, do not use other recent feature of Javascript.
We don't use any transpiler and do not intend to do so in the foreseeable future.
We will drop support for the iPad 2 at some point but for now, this means:

 - no arrow functions
 - no default or rest parameters
 - no deconstruction
 - no ellipsis in objects litterals
 - be careful: you will need to turn DOM lists (like those returned by `querySelectorAll` and `getElements*` function) into regular arrays using `Array.from`.
   This one is tricky since you don't need to do this in current versions of the browser.

Check compatibility with caniuse or MDN if unsure.

The maintainer will take care of testing your code for this, but if you wish to test the compatibility of your code and don't have a device running Safari 9 (or because a Mac is required to debug Javascript running on the iPad and you don't have one… *sigh*), you can spin up a Debian Jessie container / virtual machine and install Uzbl or Surf, which are based on a very similar version of WebKit.

On the server side, you can go wild. But we currently support Node 10, which is still maintained, so you'll need a very good reason to break compatibility with node 10.

## No external dependencies

Trivabble is very lightweight, server side as well as client side, and quite is simple to deploy.
We'd like to keep it this way.
Dependencies are a burden to maintain, especially in the JavaScript world where one module will come with hundreds of recursive dependencies. Hikes.
See how we went out of our way to implement WebSocket ourselves in the server code so we didn't have to add a dependency.
However, if you have a very good reason to add an external dependency, we are ready to reconsider, especially if the feature requiring the external dependencies is optional.
